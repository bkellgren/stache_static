define('faceCarousel',
    ['jquery', 'dcupl', 'carousel'],
    function($, dcupl) {
        $(function() {
            var $faceCarousel = $('#face-carousel'),
                $faces = $faceCarousel.find('.face');
            $faces.click(function() {
                var $self = $(this);
                // $faces.removeClass('selected');
                // $self.addClass('selected');
                //get face id
                var faceId = 1;
                dcupl.publish('faceCarousel.select', 1);
            });

            $faceCarousel.carousel({
                itemsPerPage: 3,
                extraRightPad: 2,
                extraLeftPad: 1,
                sliderLeftOffset: -60
            });
        });
    }
);
