define(
    'designs.user',
    ['jquery', 'carousel'],
    function($) {
        function chooseDesignFaceHover(evt){
            var $img = $(this).find('img');
            if(evt.type === "mouseenter") {
                $img.attr('src', $img.attr('src').replace('front', 'side'));
            }else{
                $img.attr('src', $img.attr('src').replace('side', 'front'));
            }
        }

        $(function() {
            $designCarousel = $('#choose-design-carousel');
            $designCarousel.carousel({
                extraRightPad: 2,
                extraLeftPad: 1
            });
            $designCarousel.find('.face')
                .on('mouseenter mouseleave', chooseDesignFaceHover)
                .find('img').on('mouseenter mouseleave', function(e) {
                    e.stopPropagation();
                });
            $designCarousel.find('.sprite-donate-now-small').on('click', function() {
                $('#donate-shim').find('.sprite-donate-now-small').attr('href', $(this).attr('data-donate-url'));
            });

        });
    }
);
