define(
    'design',
    ['jquery', 'template', 'lazyloadr', 'sketch', 'jquery.easing', 'jquery.scrollto', 'jquery.hashchange', 'bootstrap.tooltip', 'bootstrap.modal'],
    function($, template) {

        // on ready

        $(function(){
            // selectors
            $wrapper = $('#chosen-face');
            $design = $('#design', $wrapper);
            $designToggle = $design.find('.toggle:first');
            $designAngleToggle = $design.find('.toggle-angle:first');
            $chooseDesign = $('#choose-design', $wrapper);
            $chooseDesignToggle = $chooseDesign.find('.toggle:first');
            $chooseDesignCarousel = $('#choose-design-carousel');
            $chooseDesignCarouselControls = $chooseDesignCarousel.find('.carouselControls');
            $chooseDesignCarouselSlider = $('.carouselSlider', $chooseDesignCarousel);
            $sketchFront = $('#sketchFront', $design);
            $sketchFrontWrap = $sketchFront.parent();
            $sketchSide = $('#sketchSide', $design);
            $sketchSideWrap = $sketchSide.parent();
            $sketchForm = $('#design-form');
            $sketchControls = $('.controls', $design);
            $faceCarousel = $('#face-carousel');
            $loginNext = $('#login-next');
            $headerCarousel = $('#header-carousel');
            $sketchFormFace = $('#id_face_id');
            $submitConfirm = $('#submit-confirm');
            $donateShim = $('#donate-shim');

            // event handlers
            $designToggle.click(toggleDesign);
            $chooseDesignToggle.click(toggleChooseDesign);
            $designAngleToggle.click(toggleSketchAngle);
            $('#clear-sketch-front').click(clearSketchFront);
            $('#clear-sketch-side').click(clearSketchSide);
            $('#submit-sketch', $design).click(submitSketch);
            $('#id_title', $sketchForm).on('focus blur', clearTitleInput);
            $faceCarousel
                .on('mouseenter mouseleave', '.carouselItem', faceHover)
                .find('img').on('mouseenter mouseleave', function(e) {
                    e.stopPropagation();
                });
            $faceCarousel.carousel({
                itemsPerPage: 3,
                extraRightPad: 2
            });
            $chooseDesignCarousel
                .on('mouseenter mouseleave', '.carouselItem', chooseDesignFaceHover)
                .on('click', 'h2.copy, .choose-controls a', copyAndDonate)
                .on('mouseenter mouseleave', '.choose-controls', chooseControlsHover)
                .find('img').on('mouseenter mouseleave', function(e) {
                    e.stopPropagation();
                });
            $headerCarousel
                .on('mouseenter mouseleave', '.carouselItem img', headerCarouselFaceHover)
                .on('click', '.carouselItem img', headerCarouselItemClick);
            $submitConfirm
                .on('click', 'h2.copy, a', copyAndDonate)
                .on('click', '.close', resetPanels);
            $headerCarousel.find('img').tooltip();
            $headerCarousel.carousel({
                autoPlay: true
            });
            $donateShim
                .on('click', '.close', closeDonateShim)
                .on('mouseup', '.sprite-donate-now-small', goToDonateURL);

            $(window).resize(togglePanelWidth);
            $(window).hashchange(hashChange);

            // init
            togglePanelWidth();
            if(location.hash === ''){
                var $def = $faceCarousel.find('.carouselItem:not(.carouselPadding)').eq(1);
                chooseFace(true, $def.attr('data-face'), $def.attr('data-face-id'));
            }

            // lazy load images before setting up carousel/sketch which pull more images
            $('[data-lazy-src]', $headerCarousel).lazyloadr();
            $('[data-lazy-src]', $faceCarousel).lazyloadr();

            $(window).trigger('hashchange');

        });

        //constants
        var PANEL_CLOSED_WIDTH = 480,
            PANEL_OPEN_WIDTH = 960,
            PANEL_CLOSED_CANVAS_MARGIN = '-250px',
            PANEL_OPEN_CANVAS_MARGIN = 0,
            API_URL = '/api/designs/face/',
            NO_DESIGNS_MESSAGE = 'Nobody has drawn on this face yet, click here to draw your own design!',
            DESIGNS_LOADING_MESSAGE = '<h2>Loading Designs...</h2>',
            DESIGN_CAROUSEL_ITEM_TPL = $('#design-carousel-item').html();

        // vars
        var defaultState = true,
            carouselSetup = false,
            sketchControlsSetup = false,
            sketchedOnSide = false,
            sketchedOnFront = false,
            donateURL;

        //selectors
        var $wrapper,
            $design,
            $chooseDesign,
            $designToggle,
            $designAngleToggle,
            $chooseDesignToggle,
            $chooseDesignCarousel,
            $sketchFront,
            $sketchFrontWrap,
            $sketchSide,
            $sketchSideWrap,
            $sketchForm,
            $sketchControls,
            $chooseDesignCarouselControls,
            $faceCarousel,
            $loginNext,
            $headerCarousel,
            $sketchFormFace,
            $submitConfirm,
            $donateShim;


        function hashChange(){
            if(location.hash !== ''){
                var handle = location.hash.replace('#', '');
                chooseFace(false, handle, $faceCarousel.find('a[data-face="' + handle +'"]').attr('data-face-id'));
            }
        }

        function chooseFace(initial, face, face_id){
            if (!$faceCarousel.length) { return; }

            if(!initial){
               $.scrollTo('#chosen-face', 500);
            }
            if(!face){
               face = $(this).attr('data-face');
            }
            if(!initial){
                var $prevSelectedFace = $faceCarousel.find('.selected'),
                    $prevSelectedFaceImg = $prevSelectedFace.find('img');

                if ($prevSelectedFace.length) {
                    $prevSelectedFace.removeClass('selected');
                    $prevSelectedFaceImg.attr('src', $prevSelectedFaceImg.attr('src').replace('thumbs/', 'thumbs/bw/'));
                }
            }
            var $selectedFace = $faceCarousel.find('a[data-face="' + face +'"]'),
                $selectedFaceImg = $selectedFace.find('img');

            if (!$selectedFace.length) { return; }

            $selectedFaceImg.attr('src', $selectedFaceImg.attr('src').replace('bw/', ''));
            $selectedFace.addClass('selected');
            $loginNext.attr('value', '/#' + face);
            donateURL = $selectedFace.attr('data-donate-url');
            resetPanels();
            setupSketch(face, face_id);
            requestDesignCarousel(face_id);
        }

        function toggleDesign(){
            var $link = $(this);
            if(defaultState){
                openPanel($design);
                // animate sketch panel to center
                $sketchFront
                    .add($sketchSide)
                        .animate({
                            marginLeft: PANEL_OPEN_CANVAS_MARGIN
                        }, 500, function(){
                            $designAngleToggle.fadeIn();
                            $sketchControls.fadeIn();
                        });
                // time the head turn to begin during the animation of the panel
                if($designAngleToggle.hasClass('front')){
                    $link.animate({opacity:1}, 350, function(){
                        $designAngleToggle.trigger('click');
                    });
                }
                defaultState = false;
            }else{
                if($link.hasClass('open')){
                     resetPanels();
                }else{
                    openPanel($design);
                    // animate sketch panel to center
                    $sketchFront
                        .add($sketchSide)
                            .animate({
                                marginLeft: PANEL_OPEN_CANVAS_MARGIN
                            }, 500, function(){
                                $designAngleToggle.fadeIn();
                                $sketchControls.fadeIn();
                            });
                    // time the head turn to begin during the animation of the panel
                    if($designAngleToggle.hasClass('front')){
                        $link.animate({opacity:1}, 350, function(){
                            $designAngleToggle.trigger('click');
                        });
                    }
                }
            }
            return false;
        }

        function toggleChooseDesign(){
            var $link = $(this);
            if(defaultState){
                openPanel($chooseDesign);
                $chooseDesignCarouselControls.fadeIn();
                defaultState = false;
            }else{
                if($link.hasClass('open')){
                    resetPanels();
                }else{
                    openPanel($chooseDesign);
                    $chooseDesignCarouselControls.fadeIn();
                }
            }
            return false;
        }

        function resetPanels(){
            $design
                .add($chooseDesign)
                    .animate({
                    	width: PANEL_CLOSED_WIDTH
                    }, 500);
            $designToggle
                .add($chooseDesignToggle)
                    .removeClass('open');
            $chooseDesignCarouselControls.hide();
            $sketchControls.hide();
            $designAngleToggle.hide();
            $submitConfirm.hide();
            clearSketchFront();
            clearSketchSide();
            $('#id_title', $design).val('<title here>');
            if($designAngleToggle.hasClass('side')){
                $designAngleToggle.trigger('click');
            }
            $sketchFront
                .add($sketchSide)
                    .animate({
                        marginLeft: PANEL_CLOSED_CANVAS_MARGIN
                    }, 500);

            defaultState = true;
        }

        function openPanel($el){
            $el
                .css('z-index', '5')
                .animate({
                    width: PANEL_OPEN_WIDTH
                }, 500)
                .find('.toggle')
                    .addClass('open');
            $el.siblings()
                .css('z-index', '4')
                .animate({
                    width: PANEL_CLOSED_WIDTH
                }, 500)
                .find('.toggle')
                    .removeClass('open');
        }

        function togglePanelWidth(){
            if($(window).width() > 1400){
                $design.parent().addClass('wide');
                PANEL_OPEN_WIDTH = 1400;
                PANEL_CLOSED_WIDTH = 700;
                PANEL_OPEN_CANVAS_MARGIN = 230;
            }else{
                $design.parent().removeClass('wide');
                PANEL_CLOSED_WIDTH = 480;
                PANEL_OPEN_WIDTH = 960;
                PANEL_OPEN_CANVAS_MARGIN = 0;
            }
        }

        function setupSketch(face, face_id){
            if (!sketchControlsSetup) {
                var $fControls = $sketchFrontWrap.find('.controls a'),
                    $sControls = $sketchSideWrap.find('.controls a'),
                    $fDrawModes = $fControls.filter('[data-tool]'),
                    $sDrawModes = $sControls.filter('[data-tool]'),
                    $fSizes = $fControls.filter('[data-size]'),
                    $sSizes = $sControls.filter('[data-size]');

                $fDrawModes.on('click touchend', function(e) {
                    $fDrawModes.removeClass('active btn-inverse');
                    $(this).addClass('active btn-inverse');
                    e.preventDefault();
                });
                $sDrawModes.on('click touchend', function(e) {
                    $sDrawModes.removeClass('active btn-inverse');
                    $(this).addClass('active btn-inverse');
                    e.preventDefault();
                });
                $fSizes.on('click touchend', function(e) {
                    $fSizes.removeClass('active btn-inverse');
                    $(this).addClass('active btn-inverse');
                    e.preventDefault();
                });
                $sSizes.on('click touchend', function(e) {
                    $sSizes.removeClass('active btn-inverse');
                    $(this).addClass('active btn-inverse');
                    e.preventDefault();
                });

                sketchControlsSetup = true;
            }
            clearSketchFront();
            clearSketchSide();

            $sketchFormFace.val(face_id);
            $sketchFront.sketch();
            $sketchFront.css('background', 'url(/media/faces/front/' + face + '.png) no-repeat center top');
            preload([
                '/media/faces/front/' + face + '.png'
            ]);
            $sketchSide.sketch();
            $sketchSide.css('background', 'url(/media/faces/side/' + face + '.png) no-repeat center top');
            $('img', $designAngleToggle).attr('src', '/media/faces/side/' + face + '.png');
        }

        function toggleSketchAngle(){
            var $link = $(this);
            if($link.hasClass('side')){
                $sketchFrontWrap.hide();
                $sketchSideWrap.show();
                $link
                    .removeClass('side')
                    .addClass('front');
                var $img = $link.find('img');
                $img.attr('src', $img.attr('src').replace('side', 'front'));
            }else{
                $sketchSideWrap.hide();
                $sketchFrontWrap.show();
                $link
                    .removeClass('front')
                    .addClass('side')
                var $img = $link.find('img');
                $img.attr('src', $img.attr('src').replace('front', 'side'));
            }
            return false;
        }

        function clearSketchFront(){
            $sketchFront[0].getContext('2d').clearRect(0,0,$sketchFront[0].width,$sketchFront[0].height);
            $sketchFront.sketch('actions',[]);
            sketchedOnFront = false;
            $sketchFront.css('width', 960).unbind('mouseup.sketched').on('mouseup.sketched', function() {
                sketchedOnFront = true;
            });
            return false;
        }

        function clearSketchSide(){
            $sketchSide[0].getContext('2d').clearRect(0,0,$sketchSide[0].width,$sketchSide[0].height);
            $sketchSide.sketch('actions',[]);
            sketchedOnSide = false;
            $sketchSide.css('width', 960).unbind('mouseup.sketched').on('mouseup.sketched', function() {
                sketchedOnSide = true;
            });
            return false;
        }

        function submitSketch(evt){
            evt.preventDefault();
            if (sketchedOnFront === false) {
                alert('Please draw on the front of this face before submitting. C\'mon...');
                return false;
            }
            if (sketchedOnSide === false) {
                alert('We\'d love it if you drew on the side of this face in order to understand the full magnitude of your artistic vision. If you can\'t be bothered, just hit submit again.');
                sketchedOnSide = true;
                if ($designAngleToggle.hasClass('side')) {
                    $designAngleToggle.click();
                }
                return false;
            }

            var $title = $('#id_title', $design);
            if($title.val().toLowerCase() === '<title here>' || $title.val().toLowerCase() === ''){
                alert('Oh come on! Enter a title first.');
                return false;
            }

            var $shim = $('<div class="auth-shim"><h2>Saving...</h2></div>');
            $design.append($shim);

            var imgFront = document.createElement('img'),
                imgSide = document.createElement('img'),
                tempFront = document.createElement('canvas'),
                tempSide = document.createElement('canvas'),
                dataURLFront,
                dataURLSide;

            // create front image to data url

            tempFront.width = $sketchFront[0].width;
            tempFront.height = $sketchFront[0].height;

            tempFrontCtx = tempFront.getContext ? tempFront.getContext('2d') : null;

            imgFront.onload = function () {
                tempFrontCtx.drawImage(imgFront, 160, 0);
                tempFrontCtx.drawImage($sketchFront[0], 0, 0);
                dataURLFront = tempFront.toDataURL("image/png");

                // kick off side img creation
                imgSide.src = $sketchSide.css('background-image').replace('url(', '').replace(')', '').replace('"', '').replace('"', '');
            };
            imgFront.src = $sketchFront.css('background-image').replace('url(', '').replace(')', '').replace('"', '').replace('"', '');


            // create side image to data url

            tempSide.width = $sketchSide[0].width;
            tempSide.height = $sketchSide[0].height;

            tempSideCtx = tempSide.getContext ? tempSide.getContext('2d') : null;

            imgSide.onload = function () {
                tempSideCtx.drawImage(imgSide, 160, 0);
                tempSideCtx.drawImage($sketchSide[0], 0, 0);
                dataURLSide = tempSide.toDataURL("image/png");

                $('#id_side_string', $design).val(dataURLSide);
                $('#id_front_string', $design).val(dataURLFront);

                var design_post = $.ajax({
                    type: 'POST',
                    url: '/designs/create/',
                    data: $sketchForm.serialize(),
                    success: function(response) {
                        if(response != ''){
                            $('#success-code', $submitConfirm).html(response);
                            $design.find('.auth-shim').remove();
                            $submitConfirm.find('h4 span').html($title.val());
                            $submitConfirm.fadeIn();
                        }else{
                            alert("Sorry there was an issue, please try again :(");
                        }
                    }
                });

                design_post.error(function(){
                    alert("Sorry there was an issue, please try again :(");
                });
            };

            return false;


        }

        function requestDesignCarousel(id) {
            $.ajax({
                url: API_URL+id,
                dataType: 'json',
                error: function(jqXHR, textStatus) {
                    console.log(textStatus);
                    showNoDesigns();
                    console.log('error getting design carousel for face id '+id);
                },
                success: function(data) {
                    $chooseDesign.find('.auth-shim').remove();
                    var $shim = $('<div class="auth-shim"><h2>'+DESIGNS_LOADING_MESSAGE+'</h2></div>');
                    $chooseDesign.append($shim);
                    if (!(data instanceof Array)) {
                        showNoDesigns();
                        return;
                    }
                    var l = data.length,
                        i = 0,
                        s = '';
                    for ( ; i < l; i++) {
                        s += template(DESIGN_CAROUSEL_ITEM_TPL, data[i].fields);
                    }
                    $chooseDesignCarouselSlider.html(s);
                    $chooseDesign.find('.auth-shim').fadeOut(600, function() {
                        $(this).remove();
                    });
                    setupCarousel();
                }
            });
        }

        function copyAndDonate(){
            var $el = $(this),
                $input = $donateShim.find('input');
            $input.val($el.parent().find('h2').text().replace(' (copy)', ''));
            $donateShim.fadeIn(function(){
                $input[0].focus();
                $input[0].select();
            });
            return false;
        }

        function goToDonateURL(){
            var $link = $(this);
            $link
                .attr('href', donateURL)
                .click();
            return false;
        }

        function closeDonateShim(){
            $donateShim.hide();
            return false;
        }

        function showNoDesigns() {
            $chooseDesignCarouselSlider.empty();
            $chooseDesign.find('.auth-shim').remove();
            var $shim = $('<div class="auth-shim no-designs-found"><h2>'+NO_DESIGNS_MESSAGE+'</h2></div>').click(toggleDesign);
            $chooseDesign.append($shim);
        }
        function setupCarousel(){
            $chooseDesignCarouselSlider.css('left', 0);
            $chooseDesignCarousel.carousel({
                itemsPerPage: 2,
                extraRightPad: 1
            });
            FB.XFBML.parse();
            twttr.widgets.load();
        }

        function faceHover(evt){
            var $img = $(this).find('img'),
                $a = $img.parent();
            if(!$a.hasClass('selected')){
                if(evt.type === "mouseenter") {
                    $img.attr('src', $img.attr('src').replace('bw/', ''));
                }else{
                    $img.attr('src', $img.attr('src').replace('thumbs/', 'thumbs/bw/'));
                }
            }
            if(evt.type === "mouseenter") {
                $a.tooltip('show');
            }else{
                $a.tooltip('hide');
            }
        }

        function chooseDesignFaceHover(evt){
            var $img = $(this).find('img');
            if(!$img.attr('data-face-side') || $img.attr('data-face-side') == ''){
                return false;
            }
            if(evt.type === "mouseenter") {
                $img.attr('src', $img.attr('src').replace('front', 'side'));
            }else{
                $img.attr('src', $img.attr('src').replace('side', 'front'));
            }
        }

        function chooseControlsHover(evt){
            if(evt.type === "mouseenter") {
                $(this).css('z-index', '2');
            }else{
                $(this).css('z-index', '1');
            }
        }

        function headerCarouselFaceHover(evt){
            var $img = $(this);
            if(!$img.attr('data-face-side') || $img.attr('data-face-side') == ''){
                return false;
            }
            if(evt.type === "mouseenter") {
                $img.attr('src', $img.attr('src').replace('front', 'side'));
            }else{
                $img.attr('src', $img.attr('src').replace('side', 'front'));
            }
        }

        function headerCarouselItemClick(){
            var $img = $(this);
            location.hash = '#' + $img.attr('data-face');
            return false;
        }

        function clearTitleInput(evt){
            var $input = $(this);
            if(evt.type === "focus") {
                if($input.val().toLowerCase() === '<title here>'){
                    $input.val('');
                }
            }else{
                if($input.val().toLowerCase() === ''){
                    $input.val('<title here>');
                }
            }
        }

        function preload(arrayOfImages) {
            for(i in arrayOfImages){
                $('<img/>')[0].src = arrayOfImages[i];
            };
        }
});
