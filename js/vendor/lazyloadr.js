define('lazyloadr', ['jquery'], function($){

    // Script: jquery.lazyloadr:  lazy image loader with loading diplayed before js init
    // Version: .01
    // Author: Brett Kellgren
    //
    // Usage:
    //
    // 1. Include inline image
    // 2. Set src to blank.png (/shared/images/blank.png on jeep)
    // 3. Include data attribute and set lazySrcAttr option the same ("data-lazysrc" by default)
    // 4. Be sure to include the following css...
    //
    //      img[data-lazy-src]{
    //          display:block;
    //          background: transparent url(/shared/images/loading.gif) no-repeat 50% 25%;
    //      }
    //
    // 5. Manually call $('img[data-lazy-src'].lazyloadr(); to load lazy image
    //
    // Options:
    //
    // lazySrcAttr (str) desired data attribute to distinguish images intended to lazy load (default: "data-lazy-src")
    //

    $.fn.extend({
        lazyloadr: function(options) {
            var defaults = {
                    lazySrcAttr: 'data-lazy-src'
                },
                options =  $.extend(defaults, options),
                $body = $('body');

            return this.each(function() {
                var $el = $(this),
                    o = options,
                    cacheBuster = (getIEVersion() <= 8.0) ? '?' + new Date().getTime() : '';

                // update src to lazy url
                if ($el.attr(o.lazySrcAttr)) {
                    var $temp = $('<img/>');

                    $temp
                        .attr('src', $el.attr(o.lazySrcAttr) + cacheBuster)
                        .css({
                            position: 'absolute',
                            left: '-9999px'
                        })
                        .load(function(){
                            $el.attr('src', $el.attr(o.lazySrcAttr));
                            setTimeout(function(){
                                $el.removeAttr(o.lazySrcAttr);
                            }, 5000);
                            $temp.remove();
                        });

                    $body.append($temp);
                }
            });

            /**
             * Returns the version of Internet Explorer or a -1 (indicating the use of another browser).
             * When testing for version, use 7.0, 8.0, etc (not just 7/8).
             *
             * @usage console.log(jeep.utils.browser.getIEVersion() > 7.0);
             */
            function getIEVersion() {
                var rv = -1; // Return value assumes failure.

                if (navigator.appName === 'Microsoft Internet Explorer') {
                    var ua = navigator.userAgent;
                    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

                    if (re.exec(ua) != null) {
                        rv = parseFloat(RegExp.$1);
                    }
                }

                return rv;
            }
        }
    });

});