define('template',
	[],
	function() {
		function template(str, data) {
      for (var k in data) {
          if (data.hasOwnProperty(k)) {
              var arr = str.split('{'+k+'}');
              for(var i=0; i < arr.length; i++){
                str = str.replace('{'+k+'}', data[k]);
              }
          }
      }
      return str;
    }

    return template;
	}
);
