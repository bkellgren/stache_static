define('carousel',
    ['jquery'],
    function($) {

        /**
            @name CarouselClass
            @class
        */
        var Carousel = function(options) {
            var self = this;

            options = options || {};
            $.extend(true, this, options);

            if (!this.$) { console.warn('Carousel: no options.$ defined.'); console.log(this.$); return this; }
            if (!(this.$ instanceof $)) { console.warn('Carousel: options.$ is not instanceof jQuery'); return this; }

            this.$slider = this.$.find(this.sliderSelector);
            if (!this.$slider.length) { console.warn('Carousel: no slider node found'); return this; }

            if (!this.pageWidth) {
                this.pageWidth = this.$.outerWidth();
            }
            if (!this.itemSelector) {
                this.$items = this.$slider.children();
            } else {
                this.$items = this.$slider.find(this.itemSelector);
            }
            this.items = this.$items.length;
            if (!this.itemWidth) {
                this.itemWidth = this.$items.first().outerWidth();
            }
            if (!this.pageWidth) {
                this.pageWidth = this.$.outerWidth();
            }
            if (!this.itemsPerPage) {
                this.itemsPerPage = this.pageWidth / this.itemWidth;
            }
            //todo - look at this again...
            this.pageWidth = this.itemsPerPage * this.itemWidth;

            var maxItems = Math.floor(this.itemsPerPage);
            //console.log('maxItems = '+maxItems+', itemsPerPage = '+this.itemsPerPage);
            if (maxItems !== this.itemsPerPage) {
                this.itemsPerPage = maxItems;
                this.pageWidth = this.itemsPerPage * this.itemWidth;
                //console.log(this.pageWidth);
            }
            if (!this.pages) {
                this.pages = Math.ceil((this.items * this.itemWidth) / this.pageWidth);
            }

            this.itemSlack = (this.pages * this.itemsPerPage) - this.items;
            //console.log('itemSlack = '+this.itemSlack);

            this.$nextButton = this.$.find(this.nextButtonSelector);
            this.$prevButton = this.$.find(this.prevButtonSelector);
            if (!this.$nextButton.length || !this.$prevButton.length) { console.warn('Carousel: missing next and/or previous button. \nprevSelector: '+this.prevButtonSelector+', nextSelector: '+this.nextButtonSelector); return this; }

            if (this.pages < 2) {
                this.$nextButton.add(this.$prevButton).addClass(this.inactiveButtonClass);
                console.log('Carousel: has less than two pages. \nitems: '+this.items+', itemWidth: '+this.itemWidth+', pageWidth: '+this.pageWidth+', pages: '+this.pages);
                return this;
            }

            this.$pagesContainer = this.$.find(this.pagesSelector);
            if (this.$pagesContainer.length && this.showPages) {
                this.makePageButtons();
            }

            if (!this.infinite && this.currentPage === 1) {
                this.$prevButton.addClass(this.inactiveButtonClass);
            }

            var infinitePad = 0;
            //pad the slider to achieve infinite loop illusion
            if (this.infinite) {
                //console.log(this.extraRightPad);
                //number of extra items to clone from the start and add to the end of the slider
                var rightPad = this.itemsPerPage + this.extraRightPad;
                //number of extra items to clone from the end and add to the start of the slider
                var leftPad = this.itemsPerPage - this.itemSlack + this.extraLeftPad;
                //items from the start
                var $rightPad = this.$items.slice(0, rightPad).clone(true);
                //items from the end
                var $leftPad = this.$items.slice(-leftPad).clone(true);
                //reset slider width to accomodate extra items
                infinitePad = (rightPad + leftPad) * this.itemWidth;
                //extra width added to the start of the slider
                this.leftSlackPad = (leftPad * this.itemWidth) - this.sliderLeftOffset;
                //extra width added to the end of the slider
                this.rightSlackPad = rightPad * this.itemWidth;
                this.sliderNextWrapDistance = (this.itemsPerPage - this.itemSlack) * this.itemWidth;
                this.sliderPrevWrapDistance = this.sliderNextWrapDistance
                //console.log(this.sliderNextWrapDistance);
                //console.log(this.sliderPrevWrapDistance);
                //pixel offset of last page of the slider
                this.sliderLastPage = ((this.items + leftPad - this.itemsPerPage + this.itemSlack) * this.itemWidth) - (this.sliderLeftOffset);
                //console.log('pages = '+this.pages+', itemsper = '+this.itemsPerPage);
                //console.log('lpad = '+leftPad+', rpad = '+rightPad);
                //console.log('l = '+this.leftSlackPad+', r = '+this.rightSlackPad+', lp = '+this.sliderLastPage);
                this.$slider.prepend($leftPad).append($rightPad);
                this.$rightPad = $rightPad.addClass('carouselRightPad carouselPadding');
                this.$leftPad = $leftPad.addClass('carouselLeftPad carouselPadding');
                this.$leftPad.add(this.$rightPad).css({'float': 'left'});
            } else {
                this.leftSlackPad = -this.sliderLeftOffset;
            }
            this.$slider.css('left', ((this.pageWidth * (this.currentPage-1)) - this.leftSlackPad)+'px');

            this.$slider.width((this.pages * this.pageWidth) + infinitePad + 100);

            this.$nextButton.unbind('click.carousel').bind('click.carousel', function() {
                if (self.$nextButton.hasClass(self.inactiveButtonClass)) { return; }
                self.next();
            });
            this.$prevButton.unbind('click.carousel').bind('click.carousel', function() {
                if (self.$prevButton.hasClass(self.inactiveButtonClass)) { return; }
                self.prev();
            });

            //tabbing between inputs that aren't visible will break the carousel layout and functionality
            /*if (this.handleInputFocus) {
                this.$items.each(function(i) {
                    var $inputs = $(this).find('input, textarea').attr('tabindex', '-1').filter('input[type=text]:not(.hidden)').each(function(i) {
                        $(this).on('keydown', function(e) {
                            if (e.keyCode === 9) {
                                e.preventDefault();
                                var $next
                                if (e.shiftKey) {
                                    $next = $inputs.eq(i-1);
                                } else {
                                    $next = $inputs.eq(i+1);
                                }
                                if ($next.length) {
                                    $next.focus();
                                } else {
                                    if (e.shiftKey) {
                                        $inputs.last().focus();
                                    } else {
                                        $inputs.first().focus();
                                    }
                                }
                            }
                        });
                    });
                });
            }*/

            if (this.autoPlay) {
                this.goAuto(true);
                this.$.hover(function() {
                    self.autoPlay = false;
                }, function() {
                    self.autoPlay = true;
                    self.goAuto();
                });
            }

            this.$.addClass('activeCarousel').get(0).getCarousel = function() {
                return self;
            };
        }

        Carousel.prototype =
        /** @lends CarouselClass */
        {
            /**
                jQuery selection of the top level carousel node.
                @required
            */
            $: null,
            /**
                The amount of time for the carousel to animate between pages.
                @type Number
            */
            animationTime: 500,
            /**
                Whether or not the carousel is currently animating. Used to disable overloading next/prev page
                requests.
                @type Boolean
            */
            animating: false,
            /**
                Whether or not the carousel should automatically start scrolling through its pages or items.
                @type Boolean
                @default false
                @todo
            */
            autoPlay: false,
            autoPlayDirection: 'right',
            autoPlayHangTime: 3000,
            autoPlayTimeout: null,
            /**
                The mouse event triggered on the top level carousel node that will stop autoPlay. Any string
                that can be used with jQuery.bind is valid.
                @type String
                @default 'click'
                @todo
            */
            stopAutoOn: 'click',
            /**
                Whether or not to infinitely loop the carousel. If true and the carousel is on its last page,
                advancing will wrap the carousel to the first page.
                @type Boolean
                @default true
            */
            infinite: true,
            $rightPad: null,
            $leftPad: null,
            extraRightPad: 0,
            extraLeftPad: 0,
            /**
                Class name given to inactive next/previous buttons if infinite is false.
                @type String
                @default 'carouselButtonDisabled'
            */
            inactiveButtonClass: 'carouselButtonDisabled',
            /**
                Whether or not to automatically detect pages, pageWidth, items, itemWidth, itemsPerPage. If
                false, set all of the above properties manually. This will save init time for the carousel
                but the main purpose for "manually" setting these values is if the carousel code is somehow
                screwing it up due to unusual markup or timing.
                @type Boolean
                @default true
            */
            autoInit: true,
            /**
                The number of pages present.
                @type Number
            */
            pages: 0,
            /**
                The current page number being shown. Page 1 is the first page.
                @type Number
            */
            currentPage: 1,
            /**
                The width of each page.
                @type Number
            */
            pageWidth: 0,
            /**
                The number of pixels to offset the left egde of the slider
            */
            sliderLeftOffset: 0,
            /**
                The number of "items" in the carousel, meaning the number of distinctly different things being
                displayed in the carousel.
                @type Number
            */
            items: 0,
            currentItem: 1,
            /**
                The width of each item.
                @type Number
                @todo Special case for inconsistent item widths
            */
            itemWidth: 0,
            /**
                The number of items that fit in a single page.
                @type Number
            */
            itemsPerPage: 0,
            /**
                This number of items lacking based on the number needed to round fill out all the pages
                evenly. For example, if you have 4 items in a carousel that shows 3 items per page, the last
                page only has one item and itemSlack will be 2.
            */
            itemSlack: 0,
            rightSlackPad: 0,
            leftSlackPad: 0,
            sliderLastPage: 0,
            /**
                Tabbing between inputs that aren't visible will break the carousel layout and functionality. Set this to true to circumvent this if you have inputs in the carousel.
            */
            handleInputFocus: false,
            /**
                Whether or not to generate and update page indicators in the .carouselPages element.
            */
            showPages: true,
            /**
                The jQuery selector of the element that envelops all the items and is used to slide them.
                @type String
                @default '.carouselSlider'
            */
            sliderSelector: '.carouselSlider',
            /**
                jQuery selection of the slider element. Set in the constructor.
            */
            $slider: null,
            /**
                The jQuery selector to use to find the carousel items. Leave empty unless items are not direct
                children of the slider.
                @type String
                @default ''
            */
            itemSelector: '',
            /**
                jQuery selection of the carousel items (by default all children of the slider). Set in the
                constructor.
            */
            $items: null,
            /**
                Whether or not to advance the carousel by item. If false, the carousel is advanced by page.
            */
            pageByItem: false,
            /**
                jQuery selection of the next button. Set in the constructor using the nextButtonSelector.
            */
            $nextButton: null,
            /**
                jQuery selection of the previous button. Set in the constructor using the prevButtonSelector.
            */
            $prevButton: null,
            /**
                jQuery selector used to find the next button within the carousel.
                @type String
                @default '.carouselControlRight'
            */
            nextButtonSelector: '.carouselControlRight',
            /**
                jQuery selector used to find the previous button within the carousel.
                @type String
                @default '.carouselControlLeft'
            */
            prevButtonSelector: '.carouselControlLeft',
            /**
                Sets the next button innerHTML. Useful if you need it to say different things based on the
                carousel's page context.
                @param html Anything that can be set as an element's innerHTML
            */
            setNextButtonHtml: function(html) {
                if (!html) { return this; }
                this.$nextButton.html(html);
            },
            /**
                Sets the previous button innerHTML. Useful if you need it to say different things based on the
                carousel's page context.
                @param html Anything that can be set as an element's innerHTML
            */
            setPrevButtonHtml: function(html) {
                if (!html) { return this; }
                this.$prevButton.html(html);
            },
            /**
                Navigates the carousel forward by one page.
            */
            nextPage: function(callback, distance){
                if (this.animating === true) { return this; }
                var self = this;
                distance = distance || this.pageWidth;
                this.animating = true;
                this.$slider.animate({
                    left: '-='+distance
                }, this.animationTime, 'swing', function() {
                    if (typeof callback === 'function') { callback(); }
                    self.animating = false;
                    self.$prevButton.removeClass(self.inactiveButtonClass);
                    if (self.autoPlay) {
                        self.goAuto(true);
                    }
                });
            },
            /**
                Navigates the carousel back by one page.
            */
            prevPage: function(callback, distance) {
                if (this.animating === true) { return this; }
                var self = this;
                distance = distance || this.pageWidth;
                this.animating = true;

                this.$slider.animate({
                    left: '+='+distance
                }, this.animationTime, 'swing', function() {
                    if (typeof callback === 'function') { callback(); }
                    self.animating = false;
                    self.$nextButton.removeClass(self.inactiveButtonClass);
                    if (self.autoPlay) {
                        self.goAuto(false);
                    }
                });
            },
            /**
                Navigates the carousel forward by one item.
                @todo
            */
            nextItem: function(callback) {
                if (this.animating === true) { return this; }
                var self = this;
                distance = this.itemWidth * this.itemsPerPage;
                this.animating = true;
                this.$slider.animate({
                    left: '-='+distance
                }, this.animationTime, 'swing', function() {
                    if (typeof callback === 'function') { callback(); }
                    self.animating = false;
                    self.$prevButton.removeClass(self.inactiveButtonClass);
                    if (self.autoPlay) {
                        self.goAuto(true);
                    }
                });
            },
            /**
                Navigates the carousel back by one item.
                @todo
            */
            prevItem: function() {
                if (this.animating === true) { return this; }
                var self = this;
                distance = this.itemWidth * this.itemsPerPage;
                this.animating = true;
                this.$slider.animate({
                    left: '-='+distance
                }, this.animationTime, 'swing', function() {
                    if (typeof callback === 'function') { callback(); }
                    self.animating = false;
                    self.$prevButton.removeClass(self.inactiveButtonClass);
                    if (self.autoPlay) {
                        self.goAuto(true);
                    }
                });
            },
            /**
                Navigates the carousel forward. Based on the current page and given carousel options it
                selects the proper method (page or item) and may disable the next button.
            */
            next: function() {
                if (this.animating === true) { return this; }
                var self = this;
                self.clearAutoPlayTimeout();
                var callback = function(){};
                var distance = this.pageWidth;
                var prevPage = this.currentPage;
                this.currentPage++;
                var loop = this.currentPage > this.pages;
                //console.log('loop = '+loop+', currentPage = '+this.currentPage+', pages = '+this.pages);
                var infiniteLoop = loop && this.infinite;
                if (infiniteLoop) {
                    this.currentPage = 1;
                } else if (loop) {
                    this.currentPage = this.pages;
                    return;
                }

                if (infiniteLoop) {
                    callback = function() {
                        self.wrapNext();
                    };
                    distance = this.sliderNextWrapDistance;
                } else if (this.currentPage === this.pages && !this.infinite) {
                    this.$nextButton.addClass(this.inactiveButtonClass);
                }
                if (this.pageByItem) {
                    this.nextItem(callback);
                } else {
                    this.nextPage(callback, distance);
                }
                this.setActivePage();

                return this;
            },
            /**
                Navigates the carousel back. Based on the current page and given carousel options it
                selects the proper method (page or item) and may disable the previous button.
            */
            prev: function() {
                if (this.animating === true) { return this; }
                var self = this;
                self.clearAutoPlayTimeout();
                var callback = function(){};
                var distance = this.pageWidth;
                var prevPage = this.currentPage;
                this.currentPage--;
                var loop = this.currentPage < 1;
                //console.log('loop = '+loop+', currentPage = '+this.currentPage);
                var infiniteLoop = loop && this.infinite;
                if (infiniteLoop) {
                    this.currentPage = this.pages;
                } else if (loop) {
                    this.currentPage = 1;
                }

                if (infiniteLoop) {
                    callback = function() {
                        self.wrapPrev();
                    }
                    distance = this.sliderPrevWrapDistance;
                }
                if (this.pageByItem) {
                    this.prevItem(callback);
                } else {
                    this.prevPage(callback, distance);
                }
                this.setActivePage();

                return this;
            },
            /**
                If the carousel has gone from the first page to the last page by looping backward, this
                function "resets" the slider left value so it can seamlessly continue back through the items.
            */
            wrapPrev: function() {
                //console.log('wrap to -'+this.sliderLastPage);
                this.$slider.css('left', (-this.sliderLastPage)+'px');
            },
            /**
                If the carousel has gone from the last page to the first page by looping forward, this
                function "resets" the slider left value so it can seamlessly continue through the items.
            */
            wrapNext: function() {
                //console.log('wrap to '+this.leftSlackPad);
                this.$slider.css('left', (-this.leftSlackPad)+'px');
            },
            /**
                Navigates the carousel to a specific page from any other page.

            */
            goToPage: function(i) {
                if (i < 1 || i > this.pages) { console.warn('Carousel: goToPage: page number is out of range.'); return this; }
                if (this.currentPage === i) { console.warn('Carousel: goToPage: already on page '+i); return this; }
                var self = this;
                var left = (this.currentPage < i) ? '-=' : '+=';
                left += Math.abs(this.pageWidth * (this.currentPage - i)) + 'px';
                var p = this.currentPage;
                this.currentPage = i;

                this.animating = true;
                this.$slider.animate({
                    left: left
                }, this.animationTime, 'swing', function() {
                    self.animating = false;
                });
                this.setActivePage();

                return this;
            },
            goAuto: function(next) {
                var self = this;
                self.clearAutoPlayTimeout();
                self.autoPlayTimeout = setTimeout(function() {
                    if (self.autoPlay) {
                        if (next === true) {
                            self.next();
                        } else if (next === false) {
                            self.prev();
                        } else if (self.autoPlayDirection === 'right') {
                            self.next();
                        } else {
                            self.prev();
                        }
                    }
                }, self.autoPlayHangTime);
            },
            clearAutoPlayTimeout: function() {
                var self = this;
                if(self.autoPlayTimeout !== null) {
                    clearTimeout(self.autoPlayTimeout);
                    self.autoPlayTimeout = null;
                }
            },
            /**
                The jQuery selector used to find the container for page indicators.
                @type String
                @default '.carouselPages'
            */
            pagesSelector: '.carouselPages',
            /**
                jQuery selection of the page indicators' container found with this.pagesSelector.
            */
            $pagesContainer: null,
            /**
                jQuery selection of the page indicators generated by this carousel.
            */
            $pageButtons: null,
            /**
                HTML snippet template for the page buttons generated by this carousel. If the string "{page}"
                is found in the snippet it will be replaced with the page number.
                @type String
                @default '<img class="pageButton" src="/img/empty.gif" alt="{page}"/>'
            */
            pageButtonTemplate: '<img class="pageButton" src="/img/empty.gif" alt="{page}"/>',
            /**
                The class name to add to the active page indicator.
            */
            activePageClass: 'activePage',
            /**
                Generates page buttons for the carousel.
            */
            makePageButtons: function() {
                var self = this;
                for(var i = 0; i < this.pages; i++) {
                    this.$pagesContainer.append(this.pageButtonTemplate.replace('{page}', i));
                }
                this.$pageButtons = this.$pagesContainer.children();
                this.$pageButtons.unbind('click.carousel').on('click.carousel', function() {
                    if ($(this).hasClass(self.inactiveButtonClass)) { return; }
                    self.goToPage($(this).index()+1);
                });
                this.setActivePage();
            },
            /**
                Sets the activePageClass class name on the active page indicator.
                @private
            */
            setActivePage: function() {
                if (this.$pageButtons) {
                    this.$pageButtons.removeClass(this.activePageClass).eq(this.currentPage-1).addClass(this.activePageClass);
                }
                if (this.currentPage === 1 && !this.infinite) {
                    this.$prevButton.addClass(this.inactiveButtonClass);
                    this.$nextButton.removeClass(this.inactiveButtonClass);
                } else if (this.currentPage === this.pages && !this.infinite) {
                    this.$nextButton.addClass(this.inactiveButtonClass);
                    this.$prevButton.removeClass(this.inactiveButtonClass);
                } else {
                    this.$nextButton.removeClass(this.inactiveButtonClass);
                    this.$prevButton.removeClass(this.inactiveButtonClass);
                }
            },
            /**
                Returns a jQuery selection containing the items visible on the current page. If the carousel
                is infinite and has padding items, the items returned for the first page will include the
                padding items at the end and the items returned from the last page will include the padding
                items from the beginning.
                @param {Boolean} withPad Whether or not to include any padding items (used in infinite carousel)
                @param {Boolean} onlyVisiblePad If withPad is true, whether or not to limit the padding items
                included to only those that are visible on that page.
            */
            getCurrentPageItems: function(withPad, onlyVisiblePad) {
                var start = (this.currentPage * this.itemsPerPage) - this.itemsPerPage;
                var end = (this.currentPage * this.itemsPerPage);
                var $items = this.$items.slice(start, end);
                if (withPad) {
                    if (this.currentPage === 1 && this.$leftPad && !onlyVisiblePad) {
                        $items = $items.add(this.$leftPad);
                    } else if (this.currentPage === this.pages && this.$rightPad) {
                        if (onlyVisiblePad) {
                            var pad = end - this.items;
                            $items = $items.add(this.$rightPad.slice(0, pad));
                        } else {
                            $items = $items.add(this.$rightPad);
                        }
                    }
                }
                return $items;
            }
        }

        $.fn.carousel = function(options) {
            var $this = $(this);
            if (!$this.length) { return $(this); }
            options = options || {};
            options = $.extend(true, { $: $this }, options);
            return new Carousel(options);
        }

        return Carousel;
    }
);
