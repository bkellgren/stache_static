{
    baseUrl: "../js",
    name: "main",
    out: "../js/script-built.js",
    paths: {
        'jquery': 'vendor/jquery-1.8.2.min',
        'jquery.easing': 'vendor/jquery.easing.1.3',
        'jquery.scrollto': 'vendor/jquery.scrollTo-1.4.3.1',
        'jquery.hashchange': 'vendor/jquery.ba-hashchange',
        'bootstrap.tooltip': 'vendor/bootstrap-tooltip',
        'bootstrap.modal': 'vendor/bootstrap-modal',
        'dcupl': 'vendor/dcupl',
        'dcupl.hook': 'vendor/dcupl.hook',
        'template': 'vendor/template',
        'carousel': 'vendor/carousel',
        'lazyloadr': 'vendor/lazyloadr',
        'sketch': 'vendor/sketch',
        'design': 'modules/design',
        'designs.user': 'modules/designs.user',
        'faceCarousel': 'modules/faceCarousel'
    }
}
